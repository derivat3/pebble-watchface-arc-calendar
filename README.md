![screenshot](assets/arc-calendar_preview.png)

# ArcCalendar X v3.0.0 [-> Download](https://bitbucket.org/derivat3/pebble-watchface-arc-calendar/raw/master/src/watch-face-arc-calendar.pbw)
Highly configurable analog watchface, which solves the problem of scheduling in daily business. 
With a connected Google Calendar, you will now see upcoming appointments directly on your wrist and, if you wish, you will be notified on time.

But not enough, since version v.3.0.0 you get the possibility to fetch data via GET Requests and display them as dashboard elements on your watchface.
And because the possibilities of your customized watchface increased so much, we also offer now a way to backup and restore your watchface settings.

And ... even without a linked calendar or smart dashboards, the clock is just fun. You can create and save up to ten personal clock faces. 
And if it has to go fast, the random clock face generator will help you in this case!

## Benefits
1. All your meetings and appointments on your wrist
2. Use any public google calendar
3. Get notifications just before next appointment
4. Fetch 'Custom Values' from the internet via GET requests
5. Use variables and placeholder to manage dashboard elements
6. Backup, share and restore your personal watchface creation
7. Highly configurable watchface
8. Optimized for Pebble Round
9. 4 clock face presets
10. Memory slots for 10 custom clock faces
11. Configurable random generator
12. Configurable notes for day, weekday, month etc.

## List of Custom Settings

- Background Color
- Face Color for [Minute|Hour|Cardinal]
- Face Length for [Minute|Hour|Cardinal]
- Face Thickness for [Minute|Hour|Cardinal]
- Face Offset for [Minute|Hour|Cardinal]
- Hand Color for [Second|Minute|Hour]
- Hand Length for [Second|Minute|Hour]
- Hand Thickness for [Second|Minute|Hour]
- Arc Calendar meeting colors for [Future|Next|Current|]
- Arc Calendar meeting size
- Arc Calendar meeting alert
- Arc Calendar meeting title
- Random generator for [Color|Hand|Face]
- Notes for day, weekday, month etc.
- ...

# How to Setup Google Calendar Credentials

## Create public Google Calendar
1. Create a [Google Calendar](https://calendar.google.com/calendar) and 
   name it e.g. *pebble*
2. Open the calendar settings and make it public
3. Copy and use the Calendar-ID which should look like this:  
   xxxxxxx@group.calendar.google.com

## Create your personal Google API Key
1. Open Google [Developers Console](https://console.developers.google.com/)
2. Activate the Google Calendar API in the Google Developers Console.
3. Under Credentials, create a new Public API access key
4. Copy and use this API Key


# Changelog

## v3.0.0
- Braking change: Classic Pebble not supported ... Please use [v2.0.0](https://bitbucket.org/derivat3/pebble-watchface-arc-calendar/src/v2.x/)
- Introduce 'Custom Values' fetched via GET requests to handle cool public APIs
- Introduce variables and placeholder pattern to flexible handle the 'Custom Values'
- Introduce dashboard elements to display the 'Custom Values' in different manner
- Introduce the possibility to Export and Import watchface settings
- Update und simplify the settings page
- Add new seeting for 'Shake Events'

## v2.0.0
- Bugfix: Replace ES6 feature  

## v1.9.0
- Add optinal notes for day, weekday, month etc.
- Add config to handle over painting of minutes and hours
- Improve the usability of the settings menu

## v1.8.0
- Update the settings of the 4th watchface preset.
- Enable all day events provided by the google calendar API.
- Enable the randomizer setting ‚Update on Shake‚

## v1.7.0
- Make the watch face available for every pebble.
- Add optional vibration to the notification counter.

## v1.6.0
- Updates and Bugfixing.

## v1.5.0
- Bugfix: Calculation of the date arcs.

## v1.4.0
- Reduce watchface file size.

## v1.3.0
- Bugfix: Broken memory slots while saving and loading.

## v1.2.0
- Added a dark themed preset

## v1.1.0
- Refactor and update the calendar render method.
- Add a configuration button to refresh the calendar data manually.
- Add some new submit buttons to the clay settings.

## v1.0.0
- First Release of the ArcCalendar watchface